# Задание 1
# Создайте функцию, которая создает класс, на основе переданных ей названия, атрибутов и
# методов. Необходимо, чтобы все названия переданных атрибутов и методов были приведены к
# нижнему регистру до создания класса.


def createClass(classname, supers, classdict):
    # newdict = dict()
    # for i in classdict:
    #     newdict[i.lower()] = classdict[i]
    return type(classname, supers, {key.lower(): value for key, value in classdict.items()})


Vasya = createClass('testclass', (), {"NAME": "vasya", "AgE": 1234})

print(f"Name: {Vasya.name}")
# print(f"Surname: {vasya.surname}")
print(f"Age: {Vasya.age}")
