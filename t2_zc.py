# Задание 2
# Создайте метакласс, который проверяет класс на запрет использования цифр в именах атрибутов
# и методах, а также верхнего регистра. Генерировать исключение, если данные правила нарушены.
from string import ascii_uppercase, digits


class MetaClass(type):
    def __new__(mcs, name, bases, attrs):
        print(name, attrs)
        for key in attrs:
            if set(key) - set(digits) != set(key):
                raise TypeError("attr name consist numbers")
            if set(key) - set(ascii_uppercase) != set(key):
                raise TypeError("attr name consist uppercase")
        return super().__new__(mcs, name, bases, attrs)


class Person(metaclass=MetaClass):
    var = 12

    def __init__(self, name, surname, age):
        self.name = name
        self.surname = surname
        self.age = age


try:
    class InvalidPerson(metaclass=MetaClass):
        var = 12345
        var2 = 12345
except TypeError as e:
    print(e)

vasya = Person("name", "surname", 1234)
